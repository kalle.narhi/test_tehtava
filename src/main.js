const express = require('express');
const { interfaces } = require('mocha');
const { sum, multiply } = require('./mylib');
const app = express()
const port = 3000
const mylib = require('./mylib');

// endpoint localhost:3000/
app.get('/', (req, res) => {
    res.send('Hello World!')
})

// endpoint localhost:3000/add?a=42&b=21
app.get('/add', (req, res) => {
    const a =  parseInt(req.query.a);
    const b = parseInt(req.query.b);
    console.log({ a , b });
    const total = mylib.sum(a,b);
    res.send('add works ' + total.toString());
});

app.get('/test', (req, res) => {
    const a =  parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const minus = mylib.minus(a, b);
    const multiply = mylib.multiply(a, b);
    const division = mylib.division(a, b);
    mylib.helloWorldTimesTen();
    res.send("Tässä hieman laskutoimituksia: \n" + minus.toString() + "\n" +
    multiply.toString() + "\n" + division.toString());
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})