module.exports = {
    sum: (a,b) => {
        return a + b;
    },
    minus: (a,b) => {
        return a - b;
    },
    multiply: (a,b) => {
        return a * b;
    },
    division: (a,b) => {
        return a / b;
    },
    helloWorldTimesTen: () => {
        for(let i = 0;i < 10;i++) {
            console.log("Hello World!");
        }
    }
}