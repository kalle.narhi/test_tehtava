const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require("../src/mylib");

describe("Unit testing mylib.js", () => {

    let myvar = undefined;
    before(() => {
        myvar = 1;
    })
    it("Should return 2 when using sum function with a=1, b=1", () => {
        const result = mylib.sum(1,1);
        expect(result).to.equal(2);
    });
    it.skip("Assert foo is not bar", () => {
        assert("foo" !== "bar")
    });
    it("Multiply should return 50 when a=10, b=5", () => {
        const result = mylib.multiply(10,5);
        expect(result).to.equal(50);
    });
    it("MyVar should exist", () => {
        should.exist(myvar);
    });
    after(() => {
        console.log("Hello!");
    })
}); 